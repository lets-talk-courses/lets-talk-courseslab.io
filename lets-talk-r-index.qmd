---
listing:
  contents: [1-intro-to-r.qmd, 2-dataviz-and-ggplot.qmd, 3-coding-practices.qmd, 4-tidyverse.qmd, 5-frequentist-regression-analysis-with-r.qmd]
  sort:
    - "date desc"
    - "title"
  type: grid
  grid-item-align: left
  grid-item-border: false
  categories: true
  sort-ui: false
  fields: [author, title, date, Host, image]
  date-format: short
page-layout: full

title-block-banner: true
---

# Let's talk about R

## Goal

To provide an introduction to coding in the R programming language to carry out the statistical analyses.

## The content

It is divided in 5 blocks.

### **1. Intro to R**

The first block starts with a brief presentation of R, and covers the installation of both R and RStudio, an Integrated Development Environment (IDE) originally designed for R. It continues with an overview of RStudio and moves into simple functions.

### **2. Dataviz with ggplot2**

The goal of these slides is 1) to show how to draw plots with ggplot2, while 2) applying dataviz recommendations that would make the visualizations easier to read and more appealing. The reason why this is presented so early in the process of learning R is that users can see the results of what they are doing from the very beginning in a very clear way, while they are gradually getting used to working with R. Walking through all the slides would take too long, but users may get insights on a wide range of example plots and tools. It starts with the construction of a simple plot, a histogram, and progressively introduces other types of plots: density plots, scatter plots, violin plots, boxplots, lines, etc. Theming and how to arrange multiple plots is also covered.

### **3. Good coding practices**

Coding is an overwhelming experience for most new coders. We believe it is important to get a basic understanding on how we communicate with computers and to develop a set of basic principles that will make coding more efficient and reproducible in the short, mid, and long term. The third block of slides aims for that. They present the working directory and explain why it is important to work following a project-based approach to ensure reproducible workflows. We provide general advice and some insights into different ways of coding in R and how to comment your code. Finally, we highlight the importance of using variables that are defined before they are used in functions, instead of directly using stored values, making adjustments easier.

### **4. Intro to the tidyverse**

The tidyverse may not be the best solution for every person writing R code. Nonetheless, it allows new coders to manipulate their data quite easily. We believe it is a good starting point and let users approach base R once they get more fluent at coding. Thus, here we provide an introduction to many of the basic operations the tidyverse offers: reading, manipulating, renaming, and filtering data, selecting columns, combining tables, etc.


### **5. Intro to frequentist regression point estimation with R**.
This is often the ultimate goal of many researchers: to model the data in order to make inferences about the world. The last block of slides provides a starting point to modeling multiple types of data and extracting and visualizing the model's results. We begin with intercept-only linear regression of Gaussian processes, and escalate to multiple regression, and multiple regression with interactions. We then approach other instances of Generalized Linear Models, namely logistic and Poisson regression. Finally, we go through Multilevel Linear Regression models. Every time a model is fit to the data, we focus on extracting the estimates, visualizing them, and assessing model assumptions.

# The slides
