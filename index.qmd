
# About

This is the homepage for the "Let's talk about stats" and "Let's talk about R" courses held at the CNRS IKER laboratory in Baiona (Bayonne in French) on November 2023.

We feel that much of linguistics moving fast from reflexive abstract research to more numeric approaches and an increasing number of linguists feel the pressure to shift to statistically informed analyses. Some fields within linguistics have been using statistical tools for decades, but have not updated their workflows. Since most researchers lack the resources and the time to learn them on their own, we decided it was a good idea to provide a comprehensive introduction to statistics and coding. The materials in this site are the guide we followed to that end.

# Disclaimer

Neither of us are statisticians. We are (historical) linguists / laboratory phonologists that found themselves in big need of statistical tools to do the kind of research they wanted to do and spent long hours trying to understand them in order to perform the most accurate analyses we could. We acknowledge the fact that there will be oversimplifications, misunderstandings and even mistakes in the materials presented here. Some of them are intended, because we thought they are not relevant (at this point) for the target audience of the course. Others come from our lack of knowledge. If you spot anything that you feel that should be corrected, please, feel free to raise an issue in the gitlab repo of the course or email us directly. We are always willing to learn.

# Acknowledgents

The creators want to thank a number of institutions and individuals.

First of all, the CNRS and the ANR for funding the research project MADPAB that allowed us to develop the courses. We also want to thank IKER UMR 5478 and the researchers there for encouraging us to prepare the courses and hosting it.

Second, we are very much indebted to our precursors and the mentors in our learning process. We may not able to be exhaustive, but we have to mention Joseph V. Casillas, Stefano Coretta, Berna Devezer, Andrew Gelman, Natalia Levshina, Richard McElreath, Timo Roettger, Márton Sóskuthy, Shravan Vasishth, and Bodo Winter are some of the names to thank.

Third, we also want to thank Allison Horst for creating and sharing her beautiful illustrations of both statistical concepts and R packages and functions, and Charlotte R. Pennington for the book "A student's guide to Open Science". We used them extensively.

Finally, all our content was developed with Free and Open-Source Software (FOSS). FOSS contribute to building a more democratic society, and we should not take them for granted. We feel indebted to the developers of R and all R packages that we used, Quarto and Gitlab, and all their dependencies.



