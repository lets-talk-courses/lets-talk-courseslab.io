---
listing:
  contents: [1-basic-concepts.qmd, 2-probability-theory.qmd, correlation-and-regression.qmd, 5-open-science-and-replication-crisis.qmd]
  sort:
    - "date desc"
    - "title"
  type: grid
  grid-item-align: left
  grid-item-border: false
  categories: true
  sort-ui: false
  fields: [author, title, date, Host, image]
  date-format: short
page-layout: full

title-block-banner: true
---

# Let's talk about stats

## Goal

Provide a gentle introduction to statistics, frequentist Generalized Linear Models, and Open Science.

## The content

It is divided in 4 blocks.

### **1. Basic statistical concepts**

The first block starts with an introduction to the ways we can measure the world, the goals of statistics and (some) summary statistics of centrality and dispersion. Then, we turn to transformations (centering, normalization, log-transformation) and how they affect the data. Due to its extended use in linguistics (specially in phonetics), we also explore Lobanov normalization as a special case of normalization. After that, we introduce uniform, binomial, normal, and Poisson distributions, their relationship to natural/linguistic phenomena and how useful they are to compute expected frequencies. Finally, we present sampling, sampling distribution, standard error and confidence intervals.

### **2. Probability theory**

The second block introduces probability theory. We find probability is an oftentimes overlooked topic, yet it is very important to orient ourselves within the realm of statistics and remind us how easy it is to make mistakes while doing statistics. We start with set theory, as it allows us to understand and work with conditional probability in a more straightforward manner.

Then we turn to probability. We start with the classic examples of coin tossing and die rolling. Through simulation, we show how long-term expected frequencies match the probability of each outcome, but with the caveat that this only applies is because we specified that all outcomes were equally likely beforehand, and because we tossed a coin or rolled a die many, many times.

However, in our everyday work, we do not toss coins, but observe real phenomena. This implies that we cannot get as much data as we would like to. Sometimes, even we want to understand an event that only occurred once, or one that is already over, and we cannot get any more data of it. This leads us to present the two main schools of statistical inference, namely the frequentist and Bayesian schools. We explain conditional probability, of both independent and dependent events, and some of its properties, through Bayes' theorem.

Finally, we present three well-known examples, namely the base rate neglect, the prosecutors's fallacy and the Monty Hall problem, that are used to encourage a careful approach to statistics.

### **3. Correlation and linear regression**

This is the longest and most demanding block of the course. We start by explaining correlation and how to compute it, emphasizing not to confuse it with causation. We present illustrative cases of correlation and causation, no correlation and no causation, correlation and no causation, and no correlation and causation. Besides, we discuss that, even in cases of causation between two variables, correlation does not inform us about the direction of this causation.

Then, we turn to linear regression, and we start with middle school linear algebra as the fundamental mathematical tool to understand linear regression estimates. After that, we move into bivariate linear regression, starting with a toy example of three points that allows us to present point estimation, sum of (squared) errors and Ordinary Least Squares, and how they are calculated. We apply it to the *PalmerPenguin* dataset to check model estimates and introduce model assumptions.

After checking that the estimates and assumptions are not satisfying, we conclude that we need more variables in our analysis, so we move to multiple linear regression. We show the mathematical calculation of estimates in multiple linear regression to illustrate how multiple linear regression is not the sum of two (or more) bivariate regressions. We also introduce the interaction between independent variables. Then, we discuss that, so far, we have been working with Gaussian generative processes and extend the Generalized Linear Model framework to binomial and Poisson processes. Finally, we extend Generalized Linear Models to Multilevel Generalized Linear Models to account for repeated measures and discuss R^2^ for model performance assessment and bottom-up and top-down model selection procedures.

Finally, we present the Frequentist School of Inference. We retake sampling distributions from the first day and explain how p-values are computed. We focus on what p-values are and what they are not, to raise awareness on how easily they are misinterpreted. After that, we present Null Hypothesis Significance Testing (NHST) as a decision procedure about the null hypothesis based on p-values. Through simulation, we illustrate the dance of p-values and show that p-values above the alpha level may tell nothing in low-power situations. We end up by proposing to focus on estimates and confidence intervals rather than p-values to understand our regression analysis and the uncertainty in it.

### **4. A replication crisis and the ways out**

Our final block presents the replication crisis many scientific fields are facing (or have been facing) and some of the ways to get out. of it We present what replication is and what "results do not replicate" means. This leads us to the causes of the crisis (fishing expeditions, HARKing, underpowered studies, etc.) and how they all relate to NHST. We then provide an introduction to preregistration as a tool to deal with some of the NHST related problems and provide an introduction to Open Science as a broader framework for reproducible and transparent research. Finally, we sketch some solutions to the replication crisis that go beyond NHST, such as New Frequentist Statistics (illustrated by the work of Cummings) and Bayesian Inference.

# The slides
